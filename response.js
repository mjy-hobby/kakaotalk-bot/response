function response(room, message, sender, isGroupChat, session, imageDB)
{
    /**  카카오톡 알림을 읽으면 호출
      * @param {String} room - 방 이름
      * @param {String} message - 메세지 내용
      * @param {String} sender - 발신자 이름
      * @param {Boolean} isGroupChat - 단체채팅 여부
      * @param {Object} session - 세션 캐싱 답장 메소드 객체
      * @param {Object} imageDB - 프로필 이미지와 수신된 이미지 캐싱 객체
      * @method imageDB.getImage() - 수신된 이미지가 있을 경우 Base64 인코딩 되어있는 JPEG 이미지 반환, 기본 값 null
      * @method imageDB.getProfileImage() - Base64 인코딩 되어있는 JPEG 프로필 이미지 반환, 기본 값 null
      * @method session.reply(답장) - 메시지가 도착한 방에 답장을 보내는 메소드 */
}

let view_root;
let buttons = "a".split(",");

let btnClickListener = new android.view.View.OnClickListener()
{
    run : function onClick(view)
    {
        android.widget.Toast.makeText(Api.getContext(), view.getTag(), android.widget.Toast.LENGTH_SHORT).show();
    }
};

function onCreate(unused, activity)
{
    /**  스크립트 액티비티 onCreate
      * @param {Object} unused - onSaveInstanceState가 없으므로 쓸 데 없음
      * @param {Object} activity - 액티비티 컨텍스트 */
    view_root = new android.widget.LinearLayout(activity);
    view_root.setOrientation(android.widget.LinearLayout.VERTICAL);
    var params = new android.widget.LinearLayout.LayoutParams(
        android.widget.LinearLayout.LayoutParams.MATCH_PARENT,
        android.widget.LinearLayout.LayoutParams.MATCH_PARENT);
    view_root.setLayoutParams(params);
    var view_tl = new android.widget.TableLayout(activity);
    params = new android.widget.TableLayout.LayoutParams(
        android.widget.TableLayout.LayoutParams.MATCH_PARENT,
        android.widget.TableLayout.LayoutParams.WRAP_CONTENT);
    view_tl.setLayoutParams(params);
    var view_tr = new android.widget.TableRow(activity);
    params = new android.widget.TableRow.LayoutParams(
        android.widget.TableRow.LayoutParams.MATCH_PARENT,
        android.widget.TableRow.LayoutParams.MATCH_PARENT);
    view_tr.setLayoutParams(params);
    for(let i = 0; i < buttons.length; i++)
    {
        const btn = new android.widget.Button(activity);
        btn.setTag(i);
        btn.setText(buttons[i]);
        btn.setOnClickListener(btnClickListener);
        buttons[i] = btn;
    }
    view_tl.addView(view_tr);
    view_root.addView(view_tl);
    activity.setContentView(view_root);
}
function onStart(activity)
{
    /**  스크립트 액티비티 onStart
      * @param {Object} activity - 액티비티 컨텍스트 */
}
function onResume(activity)
{
    /**  스크립트 액티비티 onResume
      * @param {Object} activity - 액티비티 컨텍스트 */
}
function onPause(activity)
{
    /**  스크립트 액티비티 onPause
      * @param {Object} activity - 액티비티 컨텍스트 */
}
function onStop(activity)
{
    /**  스크립트 액티비티 onStop
      * @param {Object} activity - 액티비티 컨텍스트 */
}
function onDestroy(activity)
{
    /**  스크립트 액티비티 onDestroy
      * @param {Object} activity - 액티비티 컨텍스트 */
}
